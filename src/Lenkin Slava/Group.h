#pragma once
#include <string>
#include <iostream>
#include "Student.h"
class Student;
class Group
{
	friend class Student;
private:
	int name;
	int number;
	Student **st;
	Student *Head;
public:
	Group();
	Group(int name);
	void setGroup(int name);
	void addStudent(Student *s);
	void setHead();
	Student* getHead();
	int getName();
	int getNumder();
	Student* fouStudent(int id);
	Student* fouStudent(string fio);
	double midMark();
	void delStudent(int id);
	void showStudent();
	~Group();
};
