#pragma once
#include <string>
#include <iostream>
using namespace std;
class Group;
class Student
{
	friend class Group;
private:
	string fio;
	int spec;
	Group *gr;
	int year;
	int *marks;
	int number;
	int id;
public:
	Student();
	Student(string fio, int spec, int id);
	Student(const Student &st);
	Student* getStudent();
	void setStudent(string fio, int spec, int id);
	void setId(int id);
	void setGroup(Group *gr);
	void toGroup(Group *gr);
	double getmidMark();
	int getId();
	int getSpec();
	Group* getGr();
	int* getMark();
	int getNumMark();
	string getFio();
	bool changeYear();
	void addMark(int m);
	void showFio();
	void showMark();



	~Student();
};

